# common-lisp-packages-demo

## Wiki's about Common LISP
* [*Common Lisp*](https://wiki.archlinux.org/index.php/Common_Lisp)
* [*The Common Lisp Cookbook – Getting started *](https://lispcookbook.github.io/cl-cookbook/getting-started.html)

## Books about Common LISP
* [*Common LISP recipes : a problem-solution approach*](https://www.worldcat.org/title/common-lisp-recipes-a-problem-solution-approach)
  * Safari Business and Technical Books Combo Collection
  * Skillsoft Books
* [*Practical Common Lisp*](https://www.worldcat.org/title/practical-common-lisp)
  * [*Practical Common Lisp*](http://www.gigamonkeys.com/book/)

## What is lisp good for
* [what is lisp good for](https://www.google.com/search?q=what+is+lisp+good+for)
* [what is common lisp good for](https://www.google.com/search?q=what+is+common+lisp+good+for)

## Download LISP
* https://hub.docker.com/search/?q=lisp
* https://wiki.archlinux.org/index.php/Common_Lisp
* https://archlinux.org/packages/extra/x86_64/sbcl/
* https://pkgs.alpinelinux.org/packages?name=sbcl

## Download packages with Quicklisp
* https://quicklisp.org
* http://quickdocs.org

### List of packages
* [Awesome Common Lisp](https://awesome-cl.com)
* https://packages.debian.org/en/cl-
* https://packages.debian.org/sid/lisp/

## [Common Lisp Object System (CLOS)](https://en.wikipedia.org/wiki/Common_Lisp_Object_System)
* [common lisp object system](https://google.com/search?q=common+lisp+object+system)
* [*The Common Lisp Cookbook – Fundamentals of CLOS *](https://lispcookbook.github.io/cl-cookbook/clos.html)
* [*What are some examples of the power of Common Lisp Object System (CLOS)?*](https://www.quora.com/What-are-some-examples-of-the-power-of-Common-Lisp-Object-System-CLOS)
* [*Why...](https://www.quora.com/Why-is-Common-Lisp-Object-System-CLOS-considered-so-powerful-How-does-it-compare-to-OOP-features-in-other-common-languages)

### Systems inspired by CLOS
* [*The C Object System*](http://ldeniau.web.cern.ch/ldeniau/html/cos-oopsla09-draft.pdf)